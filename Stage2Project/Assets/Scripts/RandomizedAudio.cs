﻿using UnityEngine;
using UnityEngine.Audio;

public class RandomizedAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] Clips;

    [SerializeField]
    private float MinVolume = 0.5f;

    [SerializeField]
    private float MaxVolume = 1.0f;

    [SerializeField]
    private float MinPitch = 0.5f;

    [SerializeField]
    private float MaxPitch = 1.5f;

    [SerializeField]
    private AudioMixerGroup Output;

    private AudioSource mSource;

    void Awake()
    {
        mSource = GetComponent<AudioSource>();

        if (!mSource)
        {
            mSource = gameObject.AddComponent<AudioSource>();
            mSource.minDistance = 1.0f;
            mSource.maxDistance = 60.0f;
            mSource.outputAudioMixerGroup = Output;
        }
    }

    public void Play()
    {
        mSource.clip = Clips[Random.Range(0, Clips.Length - 1)];
        mSource.volume = Random.Range(MinVolume, MaxVolume);
        mSource.pitch = Random.Range(MinPitch, MaxPitch);
        mSource.Play();
    }
}