﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float MaxLifetime = 1.0f; // In seconds

    private float mLifetime;

    [SerializeField]
    private float Speed = 100.0f;

    private Rigidbody mRigidbody;

    public ParticleSystem HitParticles { get; set; }

    void Awake()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mLifetime = 0.0f;
    }

    void Update()
    {
        mLifetime += Time.deltaTime;
        if (mLifetime >= MaxLifetime)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);

        if (collision.gameObject.GetComponent<Enemy>())
        {
            var enemyPos = collision.transform.position;

            HitParticles.transform.position = new Vector3(enemyPos.x, transform.position.y, enemyPos.z) - transform.forward * 2.0f;
            HitParticles.Play();

            HitParticles.GetComponent<RandomizedAudio>().Play();
        }
    }

    public void Fire(Vector3 direction)
    {
        if (mRigidbody)
        {
            mRigidbody.AddForce(direction * Speed, ForceMode.Impulse);
        }
    }

}