﻿using UnityEngine;

public class LookAtMouse : MonoBehaviour
{
    private Camera mCamera;

    [SerializeField]
    private float RotationSpeed = 1.0f;

    private float mRotVelocity;

    void Awake()
    {
        mCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        enabled = false;
    }

    void OnPlayerEnable()
    {
        enabled = true;
    }

    void OnPlayerDisable()
    {
        enabled = false;
    }

    void Update()
    {
        RaycastHit hitInfo;
        var ray = mCamera.ScreenPointToRay(Input.mousePosition);
        bool hit = Physics.Raycast(ray, out hitInfo);

        float yaw;

        if (hit)
        {
            Vector3 direction = hitInfo.point - transform.position;
            direction.Scale(new Vector3(1.0f, 0.0f, 1.0f)); // Ignore y
            direction.Normalize();

            yaw = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
            yaw = 90.0f - yaw;
        }
        else
        {
            Vector3 screenPoint = mCamera.WorldToScreenPoint(transform.position);
            Vector3 targetPoint = Input.mousePosition;

            screenPoint.z = 0.0f;
            targetPoint.z = 0.0f;

            Vector3 direction = targetPoint - screenPoint;
            direction.Normalize();

            yaw = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            yaw = 90.0f - yaw;
        }
        
        // Interpolate towards desired rotation
        Vector3 eulerAngles = transform.eulerAngles;
        eulerAngles.y = Mathf.MoveTowardsAngle(eulerAngles.y, yaw, RotationSpeed * Time.deltaTime);
        transform.eulerAngles = eulerAngles;
    }
}