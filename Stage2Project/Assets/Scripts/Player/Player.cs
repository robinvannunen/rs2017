﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Health))]
public class Player : MonoBehaviour
{
    [SerializeField]
    private float Speed;

    [SerializeField]
    private float MaxSpeed;

    [SerializeField]
    private ParticleSystem DeadParticles;
    
    private Rigidbody mBody;
    private FollowTarget mFollowCamera;
    private ParticleSystem mDeadParticles;
    private float mDeadTimer;
    private RandomizedAudio mAudio;

    private GameManager mGameManager;

    public Health Health { get; private set; }

    void Awake()
    {
        mBody = GetComponent<Rigidbody>();

        Health = GetComponent<Health>();
        Health.OnDamaged += Health_OnDamaged;
        Health.OnDead += Health_OnDead;

        var camera = GameObject.FindGameObjectWithTag("MainCamera");
        mFollowCamera = camera.GetComponent<FollowTarget>();

        mAudio = GetComponent<RandomizedAudio>();
    }

    private void Health_OnDamaged(float amount, GameObject attacker)
    {
        if(amount > 0.0f)
        {
            mAudio.Play();
        }
    }

    private void Health_OnDead(GameObject attacker)
    {
        mBody.constraints = RigidbodyConstraints.FreezeAll;

        mDeadParticles = Instantiate(DeadParticles, transform.position + Vector3.up * 4.5f, Quaternion.identity);
        mDeadParticles.Play();

        gameObject.BroadcastMessage("OnPlayerDisable");
        mFollowCamera.Target = null;
        mDeadTimer = 0.0f;
    }

    void Start()
    {
        UIManager.Healthbar.Init(Health);

        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void OnEnable()
    {
        mBody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;

        Health.Revive();
        mFollowCamera.Target = gameObject;

        if(mDeadParticles)
        {
            Destroy(mDeadParticles);
        }

        gameObject.BroadcastMessage("OnPlayerEnable");
    }

    void OnDisable()
    {
        mFollowCamera.Target = null;

        gameObject.BroadcastMessage("OnPlayerDisable", SendMessageOptions.DontRequireReceiver);
    }

    void Update()
    {
        if(Input.GetButton("Pause"))
        {
            FindObjectOfType<ScreenManager>().Pause();
        }

        if(Health.IsDead)
        {
            if (mDeadTimer < 5.0f)
            {
                mDeadTimer += Time.deltaTime;
                if (mDeadTimer > 0.5f)
                {
                    transform.position += Vector3.down * Time.deltaTime * 10.0f;
                }
                if(mDeadTimer >= 3.0f)
                {
                    mGameManager.OnPlayerDied();
                }
            }
            return;
        }

        Vector3 direction = Vector3.zero;

        if (Input.GetKey(KeyCode.A))
        {
            direction -= Vector3.right;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            direction += Vector3.right;
        }

        if (Input.GetKey(KeyCode.W))
        {
            direction += Vector3.forward;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            direction -= Vector3.forward;
        }

        Vector3 velocity = mBody.velocity;
        velocity.Scale(new Vector3(1.0f, 0.0f, 1.0f)); // Remove y

        float currentSpeed = velocity.magnitude;
        if (currentSpeed < MaxSpeed)
        {
            mBody.AddForce(direction * Speed * Time.deltaTime);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Enemy Projectile"))
        {
            return;
        }

        Health.Damage(10.0f, collision.gameObject);
    }

    public void OnPickup(Pickup pickup)
    {
        if(pickup is HealthPickup)
        {
            Health.Heal(10.0f);
        }
    }
}
