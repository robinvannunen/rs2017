﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private GameObject BulletPrefab;

    [SerializeField]
    private float FireRate = 0.25f; // Bullets per second

    private float mTimeLastBullet;

    private ParticleSystem mHitParticles;
    private RandomizedAudio mAudio;


    void Awake()
    {
        enabled = false;

        mHitParticles = GetComponentInChildren<ParticleSystem>();
        mAudio = GetComponent<RandomizedAudio>();
    }

    void OnPlayerEnable()
    {
        enabled = true;
    }

    void OnPlayerDisable()
    {
        enabled = false;
    }

    void Update()
    {
        mTimeLastBullet += Time.deltaTime;

        if (Input.GetButton("Fire1") && mTimeLastBullet >= FireRate)
        {
            FireBullet();
        }
    }

    public void FireBullet()
    {
        mTimeLastBullet = 0.0f;

        var bullet = Instantiate(BulletPrefab, transform.position, transform.rotation).GetComponent<Bullet>();
        bullet.Fire(transform.forward);
        bullet.HitParticles = mHitParticles;
        
        mAudio.Play();
    }
}