﻿using UnityEngine;

public struct Polygon
{
    public Vector2[] points;

    public Polygon(Vector2[] points)
    {
        this.points = points;
    }

    public bool Contains(Vector2 point)
    {
        return Contains(point.x, point.y);
    }

    // http://wiki.unity3d.com/index.php/PolyContainsPoint
    public bool Contains(float x, float y)
    {
        var j = points.Length - 1;
        var inside = false;

        for (var i = 0; i < points.Length; j = i++)
        {
            if (((points[i].y <= y && y < points[j].y) || (points[j].y <= y && y < points[i].y))
            && (x < (points[j].x - points[i].x) * (y - points[i].y) / (points[j].y - points[i].y) + points[i].x))
            {
                inside = !inside;
            }
        }
        return inside;
    }
}