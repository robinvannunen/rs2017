﻿using UnityEngine;

/// <summary>
/// Modify material when entity is hurt
/// </summary>
[RequireComponent(typeof(Health))]
public class HurtAnimation : MonoBehaviour
{
    [SerializeField]
    private Material Material;

    [SerializeField]
    private Color Color = Color.red;

    [SerializeField]
    private float ActiveTime = 0.25f;

    private bool mActive;
    private float mTimer;
    
    private Material mMaterial;
    private Color mColor;

    void Awake()
    {
        mActive = false;

        mMaterial = Instantiate(Material);
        mColor = mMaterial.color;

        // Replace all materials in mesh renderer with new instance
        var renderers = GetComponentsInChildren<MeshRenderer>();
        foreach(var renderer in renderers)
        {
            if(renderer.sharedMaterial == Material)
            {
                renderer.sharedMaterial = mMaterial;
            }
        }

        var health = GetComponent<Health>();
        health.OnDamaged += Health_OnDamaged;
    }

    private void Health_OnDamaged(float amount, GameObject attacker)
    {
        if(amount <= 0.0f)
        {
            return;
        }

        mActive = true;
        mTimer = ActiveTime;
    }

    void OnDisable()
    {
        mMaterial.color = mColor;
    }

    void Update()
    {
        if(!mActive)
        {
            return;
        }

        mTimer = Mathf.Max(mTimer - Time.deltaTime, 0.0f);

        mMaterial.color = Color.Lerp(mColor, Color, mTimer / ActiveTime);

        if (mTimer == 0.0f)
        {
            mActive = false;
        }
    }
}