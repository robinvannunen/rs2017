﻿using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    public delegate void TransitionEvent();

    [SerializeField]
    private Camera ControlledCamera;

    [SerializeField]
    private string ActiveCameraView;

    private Camera[] mCameraViews;

    private TransitionEvent mCallback;
    private Camera mActiveView;
    private float mTime;

    private float mPositionTotalTime;
    private Vector3 mStartPosition;

    private float mRotationTotalTime;
    private Quaternion mStartRotation;


    void Awake()
    {
        mCameraViews = GetComponentsInChildren<Camera>();
        foreach (var camera in mCameraViews)
        {
            camera.gameObject.SetActive(false);
        }

        var firstCamera = findCameraView(ActiveCameraView);
        if (firstCamera)
        {
            ControlledCamera.transform.position = firstCamera.transform.position;
            ControlledCamera.transform.rotation = firstCamera.transform.rotation;
        }

        ControlledCamera.gameObject.SetActive(true);
        enabled = false;
    }

    public void TransitionTo(string name, float time)
    {
        TransitionTo(name, time, time, null);
    }

    public void TransitionTo(string name, float positionTime, float rotationTime)
    {
        TransitionTo(name, positionTime, rotationTime, null);
    }

    public void TransitionTo(string name, float positionTime, float rotationTime, TransitionEvent callback)
    {
        mActiveView = findCameraView(name);
        mTime = 0.0f;

        mPositionTotalTime = positionTime;
        mStartPosition = ControlledCamera.transform.position;

        mStartRotation = ControlledCamera.transform.rotation;
        mRotationTotalTime = rotationTime;

        enabled = true;
        mCallback = callback;
    }

    private Camera findCameraView(string name)
    {
        foreach (var camera in mCameraViews)
        {
            if (camera.gameObject.name.Equals(name))
            {
                return camera;
            }
        }
        return null;
    }

    private float SmoothStep(float t)
    {
        return t * t * (3 - 2 * t);
    }

    void Update()
    {
        mTime += Time.deltaTime;
        float t;

        float positionTime = Mathf.Min(mTime, mPositionTotalTime);
        t = SmoothStep(positionTime / mPositionTotalTime);
        ControlledCamera.transform.position = Vector3.Lerp(mStartPosition, mActiveView.transform.position, t);

        float rotationTime = Mathf.Min(mTime, mRotationTotalTime);
        t = SmoothStep(rotationTime / mRotationTotalTime);
        ControlledCamera.transform.rotation = Quaternion.Lerp(mStartRotation, mActiveView.transform.rotation, t);

        // Reached destination?
        if (positionTime == mPositionTotalTime && rotationTime == mRotationTotalTime)
        {
            enabled = false;

            if (mCallback != null)
            {
                mCallback();
            }
        }
    }
}