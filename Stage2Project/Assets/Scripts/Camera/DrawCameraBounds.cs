﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class DrawCameraBounds : MonoBehaviour
{
    [SerializeField]
    private Color Color = Color.white;

    private Camera mCamera;
    
    void Start()
    {
        mCamera = GetComponent<Camera>();
    }
    
    void Update()
    {
        var bounds = CameraUtils.CameraBoundingBox(mCamera);
        var length = bounds.points.Length;
        
        for (int i = 0; i < length; i++)
        {
            var p1 = bounds.points[i];
            var p2 = bounds.points[(i + 1) % length];
            Debug.DrawLine(new Vector3(p1.x, 0.0f, p1.y), new Vector3(p2.x, 0.0f, p2.y), Color);
        }
    }
}