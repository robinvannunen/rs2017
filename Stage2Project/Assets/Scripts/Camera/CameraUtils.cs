﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUtils
{
    // Calculate the frustum height at a given distance from the camera.
    public static float FrustumHeightAtDistance(float distance, float fov)
    {
        return 2.0f * distance * Mathf.Tan(fov * 0.5f * Mathf.Deg2Rad);
    }

    // Calculate the FOV needed to get a given frustum height at a given distance.
    public static float FOVForHeightAndDistance(float height, float distance)
    {
        return 2.0f * Mathf.Atan(height * 0.5f / distance) * Mathf.Rad2Deg;
    }

    // Calculate the distance for a camera with a given fov and frustum height
    public static float DistanceForFOVAndHeight(float fov, float height)
    {
        return height * 0.5f / Mathf.Tan(fov * 0.5f * Mathf.Deg2Rad);
    }

    // Calculate the viewport bounding box inside of the game
    public static Polygon CameraBoundingBox(Camera camera)
    {
        var ray = camera.ViewportPointToRay(new Vector3(0, 0, 0));
        var bottomLeft = LinePlaneIntersection(ray.origin, ray.direction);

        ray = camera.ViewportPointToRay(new Vector3(1, 0, 0));
        var bottomRight = LinePlaneIntersection(ray.origin, ray.direction);

        ray = camera.ViewportPointToRay(new Vector3(0, 1, 0));
        var topLeft = LinePlaneIntersection(ray.origin, ray.direction);

        ray = camera.ViewportPointToRay(new Vector3(1, 1, 0));
        var topRight = LinePlaneIntersection(ray.origin, ray.direction);

        return new Polygon(new Vector2[] { bottomLeft, topLeft, topRight, bottomRight });
    }

    // Intersection with the ground plane
    static Vector2 LinePlaneIntersection(Vector3 origin, Vector3 direction)
    {
        float denom = Vector3.Dot(Vector3.up, direction);
        if (Mathf.Abs(denom) > float.Epsilon)
        {
            float t = Vector3.Dot(-origin, Vector3.up) / denom;
            if (t >= 0)
            {
                var pos = origin + direction * t;
                return new Vector2(pos.x, pos.z);
            }
        }
        return origin + direction * 200.0f;
    }
}