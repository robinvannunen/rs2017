﻿using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public GameObject Target;

    [SerializeField]
    private Collider CameraBounds;

    [SerializeField]
    private float Distance = 100.0f;

    [SerializeField]
    private float InterpolationSpeed = 8.0f;

    void Update()
    {
        if (!Target)
        {
            return;
        }

        updatePosition();
    }

    private Vector3 constraintBounds(Vector3 position)
    {
        if(CameraBounds == null)
        {
            return position;
        }

        return CameraBounds.ClosestPointOnBounds(position);
    }

    private void updatePosition()
    {
        Vector3 targetPosition = Target.transform.position - transform.forward * Distance;
        targetPosition = constraintBounds(targetPosition);

        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * InterpolationSpeed);
    }
}