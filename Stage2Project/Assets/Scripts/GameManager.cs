﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum State { Paused, Playing, Gameover }

    [SerializeField]
    private Player PlayerPrefab;

    [SerializeField]
    private HealthPickup PickupPrefab;

    [SerializeField]
    private float MinPickupTime;

    [SerializeField]
    private float MaxPickupTime;

    [SerializeField]
    private WaveSpawner WaveSpawner;

    private Player mPlayer;

    private float mTimeToNextPickup;
    private List<Pickup> mPickups;
    private int mPickupCount
    {
        get
        {
            for (int i = mPickups.Count - 1; i >= 0; --i)
            {
                if (!mPickups[i]) mPickups.RemoveAt(i);
            }
            return mPickups.Count;
        }
    }

    private State mState;
    public State CurrentState { get { return mState; } }

    void Awake()
    {
        mPlayer = Instantiate(PlayerPrefab);
        mPlayer.transform.parent = transform;
        mPlayer.transform.position = new Vector3(0.0f, 0.45f, 0.0f);

        mPickups = new List<Pickup>();

        ScreenManager.OnNewGame += ScreenManager_OnNewGame;
        ScreenManager.OnPauseGame += ScreenManager_OnPauseGame;
        ScreenManager.OnResumeGame += ScreenManager_OnResumeGame;
        ScreenManager.OnExitGame += ScreenManager_OnExitGame;
    }

    void Start()
    {
        mPlayer.enabled = false;
        WaveSpawner.enabled = false;
        mState = State.Paused;
    }

    public void BeginNewGame()
    {
        mPlayer.transform.position = new Vector3(0.0f, 0.45f, 0.0f);
        mPlayer.enabled = true;
        WaveSpawner.enabled = true;
        mState = State.Playing;

        foreach (var pickup in mPickups)
        {
            Destroy(pickup.gameObject);
        }
    }

    void Update()
    {
        if (mState != State.Playing || mPlayer.Health.Value >= 1.0f || mPickupCount > 0)
        {
            return;
        }

        mTimeToNextPickup -= Time.deltaTime;
        if (mTimeToNextPickup <= 0.0f)
        {
            var position = WaveSpawner.GetRandomSpawnPosition();
            var pickup = Instantiate<Pickup>(PickupPrefab, position, Quaternion.identity);
            pickup.transform.parent = transform;
            mPickups.Add(pickup);

            mTimeToNextPickup = Random.Range(MinPickupTime, MaxPickupTime);
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0.001f;
        mState = State.Paused;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1.0f;
        mState = State.Playing;
    }

    public void EndGame()
    {
        Time.timeScale = 1.0f;
        mPlayer.enabled = false;
        WaveSpawner.enabled = false;
        mState = State.Gameover;
    }

    public void Reset()
    {
        mPlayer.transform.position = new Vector3(0.0f, 0.45f, 0.0f);
        WaveSpawner.Reset();

        foreach (var pickup in mPickups)
        {
            Destroy(pickup.gameObject);
        }
    }

    public void OnPlayerDied()
    {
        mState = State.Gameover;
        WaveSpawner.enabled = false;
        UIManager.EndGame(WaveSpawner.WaveNumber);
    }

    private void ScreenManager_OnNewGame()
    {
        BeginNewGame();
    }

    private void ScreenManager_OnPauseGame()
    {
        PauseGame();
    }

    private void ScreenManager_OnResumeGame()
    {
        ResumeGame();
    }

    private void ScreenManager_OnExitGame()
    {
        EndGame();
    }
}
