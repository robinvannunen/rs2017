﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    [SerializeField]
    private GameObject Pivot;

    [SerializeField]
    private float AnimationSpeed = 1.0f;

    [SerializeField]
    private float AnimationOffset = 1.0f;

    void OnTriggerEnter(Collider other)
    {
        if(!other.CompareTag("Player"))
        {
            return;
        }

        var player = other.GetComponent<Player>();
        player.OnPickup(this);

        Destroy(gameObject);
    }

    void Update()
    {
        Pivot.transform.localPosition = new Vector3(0.0f, Mathf.Sin(Time.time * AnimationSpeed) * AnimationOffset, 0.0f);
    }
}