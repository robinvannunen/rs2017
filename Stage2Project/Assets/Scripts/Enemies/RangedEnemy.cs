﻿using UnityEngine;

public class RangedEnemy : Enemy
{
    [SerializeField]
    private GameObject BulletPrefab;

    [SerializeField]
    private Transform GunPosition;

    public override void Attack(Player target)
    {
        Vector3 direction = target.transform.position - transform.position;
        direction.Normalize();

        var bullet = Instantiate(BulletPrefab, GunPosition.position, GunPosition.rotation).GetComponent<Bullet>();
        bullet.Fire(direction);
    }
}