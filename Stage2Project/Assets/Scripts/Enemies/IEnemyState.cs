﻿using UnityEngine;

public interface IEnemyState
{
    void Activated();

    void Deactivated();

    void UpdateState();

    void TickState();
}