﻿using UnityEngine;

public class DeadState : IEnemyState
{
    private readonly Enemy enemy;

    public Vector3 HitDirection { get; set; }

    private float mTimer;
    private bool mTransitioning;

    public readonly float WaitTime;
    public readonly float TransitionTime;

    public DeadState(Enemy parent)
    {
        enemy = parent;

        WaitTime = 5.0f;
        TransitionTime = 1.0f;
    }

    public void Activated()
    {
        // Disable all components
        foreach (var component in enemy.GetComponents<Behaviour>())
        {
            component.enabled = false;
        }
        foreach (var collider in enemy.GetComponents<Collider>())
        {
            collider.enabled = false;
        }
        enemy.Rigidbody.isKinematic = true;

        // Don't disable myself
        enemy.enabled = true;

        // Add physics to all child meshes
        foreach (var collider in enemy.GetComponentsInChildren<Collider>())
        {
            if (collider.gameObject == enemy.gameObject)
            {
                continue;
            }

            collider.enabled = true;

            var gameObject = collider.gameObject;
            gameObject.layer = LayerMask.NameToLayer("Ragdoll");

            var rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.velocity = HitDirection * Random.Range(15.0f, 25.0f);
        }
    }

    public void Deactivated()
    {
        mTimer = 0.0f;
        mTransitioning = false;
    }

    public void TickState() { }

    public void UpdateState()
    {
        mTimer += Time.deltaTime;

        if(mTimer < WaitTime)
        {
            return;
        }
        
        float time = Mathf.Min(mTimer - WaitTime, TransitionTime) / TransitionTime; // normalized

        // Go into transition state
        if (!mTransitioning)
        {
            mTransitioning = true;

            foreach (var body in enemy.GetComponentsInChildren<Rigidbody>())
            {
                if (body.gameObject == enemy.gameObject)
                {
                    continue;
                }

                body.isKinematic = true;
            }
            foreach (var collider in enemy.GetComponentsInChildren<Collider>())
            {
                collider.enabled = false;
            }
        }

        float speed = Mathf.SmoothStep(0.0f, 5.0f, time);
        enemy.transform.position += Vector3.down * Time.deltaTime * speed;

        if (time == 1.0f)
        {
            GameObject.Destroy(enemy.gameObject);
        }
    }
}