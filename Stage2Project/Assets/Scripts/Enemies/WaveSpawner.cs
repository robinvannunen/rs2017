﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaveSpawner : MonoBehaviour
{
    [System.Serializable]
    class EnemyType
    {
        public GameObject Prefab;
        public float Count;
        public float IncreaseStep;

        internal float mCountOrig;

        internal float mSpawnSteps;
        internal float mSpawnStepSize;
    }

    [SerializeField]
    private EnemyType[] EnemyTypes;

    [SerializeField]
    private int MaxEnemiesAtOnce = 50;

    [SerializeField]
    private float SpawnInterval = 1.0f; // in seconds

    [SerializeField]
    private float RestPeriod = 5.0f; // in seconds
    
    private float mWaitTimer;
    private float mSpawnStepSize;
    private float mSpawnSteps;
    private int mSpawnCount;
    private int mWaveNumber;

    private bool mActive;
    private List<Enemy> mEnemies;

    private Camera mCamera;

    void Awake()
    {
        mActive = false;
        mWaveNumber = 0;
        mWaitTimer = 0.0f;
        mEnemies = new List<Enemy>();
        
        foreach(var type in EnemyTypes)
        {
            type.mCountOrig = type.Count;
        }
    }

    public int WaveNumber { get { return mWaveNumber; } }

    void Start()
    {
        mCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Reset
    void OnEnable()
    {
        Reset();
        mActive = false;
        mWaveNumber = 0;
        mWaitTimer = 0.0f;

        foreach (var type in EnemyTypes)
        {
            type.Count = type.mCountOrig;
        }
    }

    private void newWave()
    {
        mEnemies.Clear();
        mActive = true;
        mWaveNumber++;
        mSpawnCount = 0;
        mSpawnStepSize = float.MaxValue;

        foreach (var type in EnemyTypes)
        {
            type.mSpawnStepSize = 1.0f / (Mathf.Floor(type.Count) + 1.0f);
            type.mSpawnSteps = type.mSpawnStepSize;
            mSpawnCount += (int)type.Count;

            if(type.mSpawnStepSize < mSpawnStepSize)
            {
                mSpawnStepSize = type.mSpawnStepSize;
            }
        }

        mSpawnSteps = mSpawnStepSize;
        spawnEnemy();
    }

    private void spawnEnemy()
    {
        mWaitTimer = 0;

        EnemyType selectedType = null;
        mSpawnCount--;

        for (int i = EnemyTypes.Length; i > 0; i--)
        {
            var type = EnemyTypes[i - 1];

            if (mSpawnSteps >= type.mSpawnSteps)
            {
                selectedType = type;
                if(i == 1)
                {
                    mSpawnSteps += mSpawnStepSize;
                }
                break;
            }
        }

        if(selectedType == null)
        {
            return;
        }

        selectedType.mSpawnSteps += selectedType.mSpawnStepSize;
        
        var position = GetRandomSpawnPosition();

        GameObject newEnemy = Instantiate(selectedType.Prefab, position, Quaternion.identity);
        newEnemy.transform.parent = transform;
        mEnemies.Add(newEnemy.GetComponent<Enemy>());

        // Increase health by 5% every wave
        var health = newEnemy.GetComponent<Health>();
        health.SetMaxValue(health.MaxValue * (1.0f + (mWaveNumber - 1) * 0.05f));
    }

    // Destroy still active enemies
    public void Reset()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Find random spawn location in arena that is not in camera view.
    /// And that is located on the navmesh.
    /// </summary>
    public Vector3 GetRandomSpawnPosition()
    {
        float x = 0.0f, z = 0.0f;
        var bounds = CameraUtils.CameraBoundingBox(mCamera);

        // max 100 tries (tested average to be around 2 tries total)
        for (int i = 0; i < 100; i++)
        {
            Vector2 randomPoint = 0.5f * (Arena.Min + Arena.Max) + Random.insideUnitCircle * Arena.Size.magnitude * 0.5f;

            NavMeshHit hit;
            if(NavMesh.SamplePosition(new Vector3(randomPoint.x, 0.0f, randomPoint.y), out hit, 1.0f, NavMesh.AllAreas))
            {
                x = hit.position.x;
                z = hit.position.z;

                if (!bounds.Contains(x, z))
                {
                    break;
                }
            }
        }
        return new Vector3(x, 0.5f, z);
    }

    private void endWave()
    {
        mActive = false;
        foreach (var type in EnemyTypes)
        {
            type.Count += type.IncreaseStep;
        }

        UIManager.WaveBanner.Display(mWaveNumber);
    }

    private bool allEnemiesDied()
    {
        if(mSpawnCount > 0)
        {
            return false;
        }
        foreach (var enemy in mEnemies)
        {
            if (enemy && enemy.CurrentState != enemy.DeadState)
            {
                return false;
            }
        }
        return true;
    }

    void Update()
    {
        if (mActive)
        {
            if(mSpawnCount > 0 && mEnemies.Count < MaxEnemiesAtOnce)
            {
                mWaitTimer += Time.deltaTime;
                if(mWaitTimer > SpawnInterval)
                {
                    spawnEnemy();
                }
            }
            if (allEnemiesDied())
            {
                endWave();
            }
        }
        else
        {
            mWaitTimer += Time.deltaTime;
            if (mWaitTimer > RestPeriod)
            {
                newWave();
            }
        }
    }
}