﻿using System;
using UnityEngine;

public class ChaseState : IEnemyState
{
    private readonly Enemy mEnemy;

    private readonly float mChaseRadius;
    private readonly float mAttackRadius;
    private readonly float mAttackInterval;

    private float mAttackTime;
    private Vector3 mLookAt;

    public ChaseState(Enemy parent, float chaseRadius, float attackRadius, float attackInterval)
    {
        mEnemy = parent;
        mChaseRadius = chaseRadius;
        mAttackRadius = attackRadius;
        mAttackInterval = attackInterval;
    }

    public void Activated()
    {
        mAttackTime = 0.0f;
    }

    public void Deactivated()
    {
        //
    }

    private void SwitchToPatrol()
    {
        mEnemy.CurrentState = mEnemy.PatrolState;
    }

    public void TickState()
    {
        Vector3 destination = mEnemy.Target.transform.position;
        float distance = Vector3.Distance(mEnemy.transform.position, destination);

        if (mEnemy.Target.Health.IsDead || distance > mChaseRadius)
        {
            SwitchToPatrol();
            return;
        }

        if (distance >= mAttackRadius)
        {
            Move(destination);
        }
        else
        {
            mEnemy.NavMeshAgent.ResetPath();
            mLookAt = destination;
        }
        Attack(distance);
    }

    private void RotateTowards(Vector3 target)
    {
        Vector3 direction = (target - mEnemy.transform.position).normalized;
        direction.y = 0.0f;

        Quaternion lookRotation = Quaternion.LookRotation(direction);
        float rotationSpeed = Time.deltaTime * mEnemy.NavMeshAgent.angularSpeed;
        mEnemy.transform.rotation = Quaternion.RotateTowards(mEnemy.transform.rotation, lookRotation, rotationSpeed);
    }

    public void UpdateState()
    {
        if (mAttackTime > 0.0f)
        {
            mAttackTime -= Time.deltaTime;
        }

        Vector3 destination = mEnemy.Target.transform.position;
        float distance = Vector3.Distance(mEnemy.transform.position, destination);

        if (distance < mAttackRadius)
        {
            RotateTowards(mLookAt);
        }
    }

    private void Move(Vector3 destination)
    {
        mEnemy.NavMeshAgent.destination = destination;
        mEnemy.NavMeshAgent.Resume();
    }

    private void Attack(float distance)
    {
        if (mAttackTime <= 0.0f && distance < mAttackRadius)
        {
            mEnemy.Attack(mEnemy.Target);
            mAttackTime = mAttackInterval;
        }
    }
}