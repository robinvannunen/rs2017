﻿using UnityEngine;

public class PatrolState : IEnemyState
{
    private readonly Enemy mEnemy;

    public readonly float mChaseDistance;

    public PatrolState(Enemy parent, float chaseDistance)
    {
        mEnemy = parent;
        mChaseDistance = chaseDistance;
    }

    public void Activated()
    {
        SetRandomDestination();
    }

    public void Deactivated()
    {
        //
    }

    private void SwitchToChase()
    {
        mEnemy.CurrentState = mEnemy.ChaseState;
    }
    
    public void TickState()
    {
        // Update destination
        if (mEnemy.NavMeshAgent.remainingDistance <= mEnemy.NavMeshAgent.stoppingDistance && !mEnemy.NavMeshAgent.pathPending)
        {
            SetRandomDestination();
        }

        // Check player distance
        float distance = Vector3.Distance(mEnemy.transform.position, mEnemy.Target.transform.position);
        if (distance <= mChaseDistance)
        {
            SwitchToChase();
        }
    }

    public void UpdateState() { }

    void SetRandomDestination()
    {
        float x = Arena.Min.x + Arena.Size.x * Random.value;
        float z = Arena.Min.y + Arena.Size.y * Random.value;
        var destination = new Vector3(x, 0.5f, z);

        mEnemy.NavMeshAgent.destination = destination;
        mEnemy.NavMeshAgent.Resume();
    }
}