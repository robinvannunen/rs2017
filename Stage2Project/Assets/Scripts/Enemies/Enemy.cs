﻿using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public IEnemyState CurrentState
    {
        get
        {
            return mCurrentState;
        }
        set
        {
            mNextState = value;
        }
    }
    private IEnemyState mNextState;
    private IEnemyState mCurrentState;
    private float mNextUpdate;

    private Health mHealth;

    [SerializeField]
    private float ChaseRadius;

    [SerializeField]
    private float AttackRadius;

    [SerializeField]
    private float AttackInterval;

    public PatrolState PatrolState { get; private set; }
    public ChaseState ChaseState { get; private set; }
    public DeadState DeadState { get; private set; }
    
    public NavMeshAgent NavMeshAgent { get; private set; }
    public Player Target { get; private set; }
    public Rigidbody Rigidbody { get; private set; }

    void Awake()
    {
        mHealth = GetComponent<Health>();
        mHealth.OnDamaged += MHealth_OnDamaged;
        mHealth.OnDead += MHealth_OnDead;

        Rigidbody = GetComponent<Rigidbody>();
        NavMeshAgent = GetComponent<NavMeshAgent>();
        Target = FindObjectOfType<Player>();

        PatrolState = new PatrolState(this, ChaseRadius);
        ChaseState = new ChaseState(this, ChaseRadius, AttackRadius, AttackInterval);
        DeadState = new DeadState(this);
    }

    private void MHealth_OnDamaged(float amount, GameObject attacker)
    {
        Vector3 hitDirection;
        float force = 10.0f + amount * 0.25f;

        if (attacker)
        {
            hitDirection = attacker.transform.forward;
            hitDirection.y = 0.0f;
            hitDirection.Normalize();
            
            var velocity = hitDirection * force;
            Rigidbody.velocity = velocity;
        }
    }

    protected void MHealth_OnDead(GameObject attacker)
    {
        Vector3 hitDirection = Vector3.down;

        if (attacker)
        {
            hitDirection = attacker.transform.forward;
            hitDirection.y = 0.0f;
            hitDirection.Normalize();
        }

        CurrentState = DeadState;
        DeadState.HitDirection = hitDirection;
    }

    void Start()
    {
        mCurrentState = PatrolState;
        mCurrentState.Activated();

        // Set random start time for state update.
        // This adds reaction time to AI decision making.
        // Also makes sure not all enemies update at the same time.
        mNextUpdate = 0.8f * Random.value;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Projectile"))
        {
            return;
        }

        mHealth.Damage(20.0f, collision.gameObject);
    }

    void Update()
    {
        if (mCurrentState != null)
        {
            mNextUpdate -= Time.deltaTime;
            if (mNextUpdate <= 0.0f)
            {
                mNextUpdate = 0.8f;
                mCurrentState.TickState();
            }
            mCurrentState.UpdateState();
        }

        if (mNextState != null)
        {
            var lastState = mCurrentState;
            mCurrentState = mNextState;
            mNextState = null;

            lastState.Deactivated();
            mCurrentState.Activated();
        }
    }

    public virtual void Attack(Player target)
    {
        target.Health.Damage(10.0f, gameObject);
    }
}