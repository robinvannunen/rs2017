﻿using UnityEngine;

public class WrapPosition : MonoBehaviour
{


    void Awake()
    {

    }

    void Update()
    {
        Vector3 position = transform.position;

        if (position.x < Arena.Min.x)
        {
            position.x += Arena.Size.x;
        }
        else if (position.x > Arena.Max.x)
        {
            position.x -= Arena.Size.x;
        }

        if (position.z < Arena.Min.y)
        {
            position.z += Arena.Size.y;
        }
        else if (position.z > Arena.Max.y)
        {
            position.z -= Arena.Size.y;
        }

        transform.position = position;
    }
}
