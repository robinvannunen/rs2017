﻿using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    [SerializeField]
    private Health Health;

    [SerializeField]
    private Image Image;

    [SerializeField]
    private float UpdateSpeed = 10.0f;

    private float mTargetValue;

    [SerializeField]
    private float DisplayTime = 0.5f; // In seconds

    [SerializeField]
    private float FadeTime = 1.0f; // In seconds

    private float mDisplayTime;
    private CanvasGroup mCanvasGroup;

    void Start()
    {
        Health.OnDead += Health_OnDead;
        Health.OnDamaged += Health_OnDamaged;

        mCanvasGroup = GetComponent<CanvasGroup>();
        mCanvasGroup.alpha = 0.0f;
        mDisplayTime = 0.0f;

        mTargetValue = Health.Value;
    }

    private void Health_OnDead(GameObject attacker)
    {
        Destroy(gameObject);
    }

    private void Health_OnDamaged(float amount, GameObject attacker)
    {
        mTargetValue = Health.Value;
        mDisplayTime = FadeTime + DisplayTime;
    }

    void Update()
    {
        if(mDisplayTime == 0.0f)
        {
            return;
        }

        mCanvasGroup.alpha = Mathf.Min(mDisplayTime, FadeTime) / FadeTime;
        mDisplayTime = Mathf.Max(0.0f, mDisplayTime - Time.deltaTime);

        if (Image.fillAmount != mTargetValue)
        {
            Image.fillAmount = Mathf.Lerp(Image.fillAmount, mTargetValue, Time.deltaTime * UpdateSpeed);
        }
    }
}