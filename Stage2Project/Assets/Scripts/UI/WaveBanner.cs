﻿using UnityEngine;
using UnityEngine.UI;

public class WaveBanner : MonoBehaviour
{
    [SerializeField]
    private Text Text;

    [SerializeField]
    private float DisplayTime = 4.0f;

    private float mDisplayTimer;
    private bool mActive;

    void Awake()
    {
        mActive = false;
        gameObject.SetActive(mActive);
    }

    public void Display(int waveNumber)
    {
        Text.text = string.Format("Wave {0}", waveNumber);
        mDisplayTimer = DisplayTime;

        mActive = true;
        gameObject.SetActive(mActive);
    }

    void Update()
    {
        if (!mActive)
        {
            return;
        }

        mDisplayTimer -= Time.deltaTime;
        if (mDisplayTimer <= 0.0f)
        {
            mActive = false;
            gameObject.SetActive(mActive);
        }
    }
}