﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthbar : MonoBehaviour
{
    [SerializeField]
    private Image Image;

    [SerializeField]
    private float UpdateSpeed = 10.0f;

    private float mTargetValue;

    private Health mHealth;

    public void Init(Health health)
    {
        mHealth = health;
        mHealth.OnDamaged += Health_OnDamaged;

        mTargetValue = mHealth.Value;
    }
    
    private void Health_OnDamaged(float amount, GameObject attacker)
    {
        mTargetValue = mHealth.Value;
    }

    void Update()
    {
        if (Image.fillAmount != mTargetValue)
        {
            Image.fillAmount = Mathf.Lerp(Image.fillAmount, mTargetValue, Time.deltaTime * UpdateSpeed);
        }
    }
}