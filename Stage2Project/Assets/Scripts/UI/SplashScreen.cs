﻿using UnityEngine;

public class SplashScreen : MonoBehaviour
{
    [SerializeField]
    private SceneLoader Loader;

    [SerializeField]
    private GameObject Background;

    [SerializeField]
    private CanvasGroup Fader;

    [SerializeField]
    private float TransitionTime = 0.5f;

    [SerializeField]
    private float DisplayTime = 1.5f;

    [SerializeField]
    private GameObject[] Splashes;

    enum State { Active, FadeIn, FadeOut };

    private int mIndex;
    private State mState;
    private float mTimer;
    private bool mDone;

    void Awake()
    {
        mIndex = 0;
        mState = State.FadeIn;
        mTimer = TransitionTime;
        mDone = false;

        Fader.alpha = 1.0f;
        Background.SetActive(true);

        foreach (var splash in Splashes)
        {
            splash.SetActive(false);
        }
    }

    void Start()
    {
        Splashes[mIndex].SetActive(true);
    }

    void Update()
    {
        if(mDone && (Loader.Loading || mState == State.Active))
        {
            return;
        }

        mTimer = Mathf.Max(mTimer - Time.deltaTime, 0.0f);

        if (mState == State.FadeIn)
        {
            Fader.alpha = mTimer / TransitionTime;

            if (mTimer == 0.0f)
            {
                mState = State.Active;

                if (mDone)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    mTimer = DisplayTime;
                }
            }
        }

        if (mState == State.Active && mTimer == 0.0f)
        {
            mState = State.FadeOut;
            mTimer = TransitionTime;
        }

        if (mState == State.FadeOut)
        {
            Fader.alpha = 1.0f - (mTimer / TransitionTime);

            if (mTimer == 0.0f)
            {
                mState = State.FadeIn;
                mTimer = TransitionTime;

                Splashes[mIndex].SetActive(false);

                if (++mIndex == Splashes.Length)
                {
                    mDone = true;
                    Background.SetActive(false);
                }
                else
                {
                    Splashes[mIndex].SetActive(true);
                }
            }
        }
    }
}