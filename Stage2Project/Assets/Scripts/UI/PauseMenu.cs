﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    [SerializeField]
    private ScreenManager ScreenManager;

    [SerializeField]
    private GameObject ControlsPanel;

    [SerializeField]
    private GameObject SettingsPanel;

    private CameraTransition mCameraTransition
    {
        get
        {
            return FindObjectOfType<CameraTransition>();
        }
    }

    public void Continue()
    {
        ScreenManager.Resume();

        ControlsPanel.SetActive(false);
        SettingsPanel.SetActive(false);
    }

    public void Menu()
    {
        ScreenManager.CloseAll();

        var gameManager = FindObjectOfType<GameManager>();
        mCameraTransition.TransitionTo("Menu", 3.0f, 3.0f, gameManager.Reset);

        ControlsPanel.SetActive(false);
        SettingsPanel.SetActive(false);
    }

    public void Controls()
    {
        ControlsPanel.SetActive(!ControlsPanel.activeInHierarchy);
        SettingsPanel.SetActive(false);
    }

    public void Settings()
    {
        SettingsPanel.SetActive(!SettingsPanel.activeInHierarchy);
        ControlsPanel.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}