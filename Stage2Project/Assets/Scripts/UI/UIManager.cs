﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;

    [SerializeField]
    private PlayerHealthbar _Healthbar;
    public static PlayerHealthbar Healthbar { get { return instance._Healthbar; } }

    [SerializeField]
    private WaveBanner _WaveBanner;
    public static WaveBanner WaveBanner { get { return instance._WaveBanner; } }
    
    void Awake()
    {
        instance = this;
    }

    public static void EndGame(int waveNumber)
    {
        FindObjectOfType<ScreenManager>().EndGame(waveNumber);
    }
}