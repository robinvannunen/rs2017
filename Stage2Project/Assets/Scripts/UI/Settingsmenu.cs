﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settingsmenu : MonoBehaviour
{
    [System.Serializable]
    class GameSettings
    {
        public int resolution;
        public bool fullscreen;
        public int graphics;
        public float music;
    }

    [SerializeField]
    private Dropdown ResolutionDropdown;

    [SerializeField]
    private Toggle FullscreenToggle;

    [SerializeField]
    private Dropdown GraphicsDropdown;

    [SerializeField]
    private Slider MusicSlider;

    [SerializeField]
    private AudioMixer MasterMixer;

    private Resolution[] mResolutions;

    private delegate void SettingsEvent();
    private static SettingsEvent OnSettingsUpdated;
    private static GameSettings mGameSettings;

    void Start()
    {
        // Setup resolution list
        List<Resolution> resolutions = new List<Resolution>();
        foreach (var resolution in Screen.resolutions)
        {
            if (!resolutions.Contains(resolution))
            {
                resolutions.Add(resolution);
            }
        }

        resolutions.Sort((r1, r2) => r1.width - r2.width);
        mResolutions = resolutions.ToArray();

        foreach (var resolution in mResolutions)
        {
            ResolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }

        // Setup graphics list
        foreach (var name in QualitySettings.names)
        {
            GraphicsDropdown.options.Add(new Dropdown.OptionData(name));
        }

        // Load settings
        if (mGameSettings == null)
        {
            if (LoadSettings() == false)
            {
                mGameSettings = new GameSettings();
                mGameSettings.resolution = resolutions.FindIndex(
                    c => c.width == Screen.currentResolution.width && c.height == Screen.currentResolution.height);
                mGameSettings.fullscreen = Screen.fullScreen;
                mGameSettings.graphics = QualitySettings.GetQualityLevel();
                MasterMixer.GetFloat("Volume", out mGameSettings.music);
                SaveSettings();
            }
        }

        Settings_OnUpdated();

        // Register listeners
        ResolutionDropdown.onValueChanged.AddListener(OnResolutionChanged);
        FullscreenToggle.onValueChanged.AddListener(OnFullscreenToggle);
        GraphicsDropdown.onValueChanged.AddListener(OnGraphicsChanged);
        MusicSlider.onValueChanged.AddListener(OnMusicChanged);
        OnSettingsUpdated += Settings_OnUpdated;
    }

    void OnResolutionChanged(int value)
    {
        var resolution = mResolutions[value];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        mGameSettings.resolution = value;

        OnSettingsUpdated();
        SaveSettings();
    }

    void OnFullscreenToggle(bool value)
    {
        Screen.fullScreen = value;
        mGameSettings.fullscreen = value;

        OnSettingsUpdated();
        SaveSettings();
    }

    void OnGraphicsChanged(int value)
    {
        QualitySettings.SetQualityLevel(value);
        mGameSettings.graphics = value;

        OnSettingsUpdated();
        SaveSettings();
    }

    void OnMusicChanged(float value)
    {
        MasterMixer.SetFloat("Volume", value);
        mGameSettings.music = value;

        OnSettingsUpdated();
        SaveSettings();
    }

    void Settings_OnUpdated()
    {
        ResolutionDropdown.value = mGameSettings.resolution;
        FullscreenToggle.isOn = mGameSettings.fullscreen;
        GraphicsDropdown.value = mGameSettings.graphics;
        MusicSlider.value = mGameSettings.music;

        ResolutionDropdown.RefreshShownValue();
        GraphicsDropdown.RefreshShownValue();
    }

    void SaveSettings()
    {
        string data = JsonUtility.ToJson(mGameSettings, true);
        File.WriteAllText(Application.persistentDataPath + "/settings.json", data);
    }

    bool LoadSettings()
    {
        if (!File.Exists(Application.persistentDataPath + "/settings.json"))
        {
            return false;
        }

        var data = File.ReadAllText(Application.persistentDataPath + "/settings.json");
        mGameSettings = JsonUtility.FromJson<GameSettings>(data);

        if (mGameSettings.resolution >= 0 && mGameSettings.resolution < mResolutions.Length)
        {
            var resolution = mResolutions[mGameSettings.resolution];
            Screen.SetResolution(resolution.width, resolution.height, mGameSettings.fullscreen);
        }

        if (mGameSettings.graphics >= 0 && mGameSettings.graphics < QualitySettings.names.Length)
        {
            QualitySettings.SetQualityLevel(mGameSettings.graphics);
        }

        MasterMixer.SetFloat("Volume", mGameSettings.music);

        return true;
    }
}