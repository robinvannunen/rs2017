﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private ScreenManager ScreenManager;

    private CameraTransition mCameraTransition
    {
        get
        {
            return FindObjectOfType<CameraTransition>();
        }
    }

    public void Play()
    {
        mCameraTransition.TransitionTo("Game", 5.0f, 3.0f, ScreenManager.StartGame);
    }

    public void Menu()
    {
        mCameraTransition.TransitionTo("Menu", 1.0f);
    }

    public void Controls()
    {
        mCameraTransition.TransitionTo("Controls", 1.0f);
    }

    public void Settings()
    {
        mCameraTransition.TransitionTo("Settings", 1.0f);
    }

    public void Quit()
    {
        Application.Quit();
    }
}