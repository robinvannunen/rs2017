﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    public delegate void GameEvent();
    public static event GameEvent OnNewGame;
    public static event GameEvent OnPauseGame;
    public static event GameEvent OnResumeGame;
    public static event GameEvent OnExitGame;

    public enum Screens { TitleScreen, GameScreen, PauseScreen, ResultScreen, NumScreens }

    [SerializeField]
    private Text EndGame_WaveNumber;
    
    private Canvas [] mScreens;
    private Screens mCurrentScreen;
    
    void Awake()
    {
        mScreens = new Canvas[(int)Screens.NumScreens];
        Canvas[] screens = GetComponentsInChildren<Canvas>();
        for (int count = 0; count < screens.Length; ++count)
        {
            for (int slot = 0; slot < mScreens.Length; ++slot)
            {
                if (mScreens[slot] == null && ((Screens)slot).ToString() == screens[count].name)
                {
                    mScreens[slot] = screens[count];
                    break;
                }
            }
        }

        for (int screen = 1; screen < mScreens.Length; ++screen)
        {
            mScreens[screen].enabled = false;
        }

        mCurrentScreen = Screens.TitleScreen;
    }

    public void StartGame()
    {
        if(OnNewGame != null)
        {
            OnNewGame();
        }

        TransitionTo(Screens.GameScreen);
    }

    public void Pause()
    {
        if (OnPauseGame != null)
        {
            OnPauseGame();
        }

        TransitionTo(Screens.PauseScreen);
    }

    public void Resume()
    {
        if (OnResumeGame != null)
        {
            OnResumeGame();
        }

        TransitionTo(Screens.GameScreen);
    }

    public void EndGame(int waveNumber)
    {
        if (OnExitGame != null)
        {
            OnExitGame();
        }

        EndGame_WaveNumber.text = waveNumber.ToString();
        TransitionTo(Screens.ResultScreen);
    }

    public void CloseAll()
    {
        if (OnExitGame != null)
        {
            OnExitGame();
        }

        mScreens[(int)mCurrentScreen].enabled = false;
        mCurrentScreen = Screens.TitleScreen;
    }

    private void TransitionTo(Screens screen)
    {
        mScreens[(int)mCurrentScreen].enabled = false;
        mScreens[(int)screen].enabled = true;
        mCurrentScreen = screen;
    }
}
