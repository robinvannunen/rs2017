﻿using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    void Update()
    {
        Vector3 direction = Camera.main.transform.position - transform.position;
        direction.Normalize();
        
        float pitch = 90.0f * Vector3.Dot(Vector3.up, direction);
        transform.eulerAngles = new Vector3(pitch, 0.0f, 0.0f);
    }
}