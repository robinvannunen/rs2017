﻿using UnityEngine;

public class RandomStartPosition : MonoBehaviour
{
    void Start()
    {
        float x = Arena.Min.x + Arena.Size.x * Random.value;
        float z = Arena.Min.y + Arena.Size.y * Random.value;
        transform.position = new Vector3(x, 0.5f, z);
    }
}