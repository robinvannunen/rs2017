﻿using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    private float _MaxValue = 100.0f;
    public float MaxValue { get { return _MaxValue; } }

    private float mValue;

    private bool mDead;

    public delegate void DamageEvent(float amount, GameObject attacker);
    public event DamageEvent OnDamaged = delegate { };

    public delegate void DeadEvent(GameObject attacker);
    public event DeadEvent OnDead = delegate { };

    void Awake()
    {
        Revive();
    }

    public bool IsDead { get { return mDead; } }

    public float Value { get { return mValue / MaxValue; } }

    public void Damage(float amount, GameObject attacker)
    {
        if (mDead)
        {
            return;
        }

        mValue = Mathf.Max(Mathf.Min(mValue - amount, MaxValue), 0.0f);
        OnDamaged(amount, attacker);

        if (mValue == 0.0f)
        {
            SetDead(attacker);
        }
    }

    public void Heal(float amount)
    {
        Damage(-amount, null);
    }

    public void SetDead(GameObject attacker)
    {
        mDead = true;
        OnDead(attacker);
    }

    public void SetMaxValue(float maxValue)
    {
        mValue = Value * maxValue;
        _MaxValue = maxValue;
    }

    public void Revive()
    {
        mDead = false;
        mValue = MaxValue;
        OnDamaged(.0f, null);
    }
}