﻿using UnityEngine;

[ExecuteInEditMode]
public class Arena : MonoBehaviour
{
    [SerializeField]
    private Collider ArenaBounds;

    [SerializeField]
    private bool DrawBorder = false;

    public static Vector2 Min { get; private set; }
    public static Vector2 Max { get; private set; }
    public static Vector2 Size { get; private set; }

    void Awake()
    {
        Calculate();
    }

    void Update()
    {
        if (DrawBorder)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                Calculate();
            }
#endif
            Debug.DrawLine(new Vector3(Min.x, 0.0f, Min.y), new Vector3(Min.x, 0.0f, Max.y), Color.yellow);
            Debug.DrawLine(new Vector3(Max.x, 0.0f, Min.y), new Vector3(Max.x, 0.0f, Max.y), Color.yellow);
            Debug.DrawLine(new Vector3(Min.x, 0.0f, Min.y), new Vector3(Max.x, 0.0f, Min.y), Color.yellow);
            Debug.DrawLine(new Vector3(Min.x, 0.0f, Max.y), new Vector3(Max.x, 0.0f, Max.y), Color.yellow);
        }
    }

    public void Calculate()
    {
        if(ArenaBounds == null)
        {
            return;
        }
        
        Min = new Vector3(ArenaBounds.bounds.min.x, ArenaBounds.bounds.min.z);
        Max = new Vector3(ArenaBounds.bounds.max.x, ArenaBounds.bounds.max.z);
        Size = Max - Min;
    }
}
